package code.smsbackup;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private TextView mTextMessage;
    TextView txt1;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    File file;

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults
    ) {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = (Button)findViewById(R.id.button1);
        btn2 = (Button)findViewById(R.id.button2);
        btn3 = (Button)findViewById(R.id.button3);
        btn4 = (Button)findViewById(R.id.button4);
        txt1 = (TextView)findViewById(R.id.text1);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            int requestCode = 111;
            int hasPermission = 0;
            hasPermission = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, requestCode);
            }
            requestCode = 222;
            hasPermission = 0;
            hasPermission = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
            }
            requestCode = 333;
            hasPermission = 0;
            hasPermission = checkSelfPermission(Manifest.permission.READ_SMS);
            if (hasPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {Manifest.permission.READ_SMS}, requestCode);
            }

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                ) {
                Toast.makeText(MainActivity.this, "All permissions granted", Toast.LENGTH_LONG).show();
            } else {
                requestCode = 999;
                requestPermissions(new String[] {Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_SMS}, requestCode);
                Toast.makeText(MainActivity.this, "Permissions missing", Toast.LENGTH_LONG).show();
            }

        }


        File dir = null;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.Q) {
            dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        } else {
            dir = getExternalFilesDir(null);
        }
        if (!dir.isDirectory()) {
            displayError("'Download' folder not found");
            return;
        }

        file = new File(dir, "SMS_output.csv");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            try {
                MediaScannerConnection.scanFile(this,
                        new String[]{file.toString()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                                Log.i("ExternalStorage", "Scanned " + path + ":");
                                Log.i("ExternalStorage", "-> uri=" + uri);
                            }
                        });
            } catch (Exception e) { }
        }

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt1.setText(new String("Backup as |SV at: " + file.toString()));

                //Write to file
                FileWriter fileWriter = null;
                try {

                    fileWriter = new FileWriter(file);

                    SMSHelpers smsHelpers = new SMSHelpers(MainActivity.this);
                    List<String> msgLst = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                        msgLst = smsHelpers.getAllSms(true);
                    } else {
                        msgLst = smsHelpers.getAllSmsAPIBefore19(true);
                    }

                    if (msgLst.size() <= 0) {
                        displayError("No SMS in Inbox");
                        return;
                    }

                    String delimiter = "|";
                    fileWriter.write("---"+delimiter+" Start of Messages "+delimiter+"---\n");
                    int column = 1;
                    for (String msg: msgLst) {
                        if (column > 3) {
                            column = 1;
                            fileWriter.append("\n");
                        }
                        fileWriter.append(msg + delimiter);
                        column += 1;
                    }
                    fileWriter.append("\n---"+delimiter+" End of messages "+delimiter+"---");

                } catch (IOException e) {
                    //Handle exception
                    displayError(e.getMessage().toString());
                } finally {
                    try {
                        fileWriter.close();
                    } catch (Exception e) {
                        //if not initialized its not a problem
                    }
                }

            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt1.setText(new String("Backup as ;;;SV at: " + file.toString()));

                //Write to file
                FileWriter fileWriter = null;
                try {

                    fileWriter = new FileWriter(file);

                    SMSHelpers smsHelpers = new SMSHelpers(MainActivity.this);
                    List<String> msgLst = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                        msgLst = smsHelpers.getAllSms(true);
                    } else {
                        msgLst = smsHelpers.getAllSmsAPIBefore19(true);
                    }

                    if (msgLst.size() <= 0) {
                        displayError("No SMS in Inbox");
                        return;
                    }

                    String delimiter = ";;;";
                    fileWriter.write("---"+delimiter+" Start of Messages "+delimiter+"---\n");
                    int column = 1;
                    for (String msg: msgLst) {
                        if (column > 3) {
                            column = 1;
                            fileWriter.append("\n");
                        }
                        fileWriter.append(msg + delimiter);
                        column += 1;
                    }
                    fileWriter.append("\n---"+delimiter+" End of messages "+delimiter+"---");

                } catch (IOException e) {
                    //Handle exception
                    displayError(e.getMessage().toString());
                } finally {
                    try {
                        fileWriter.close();
                    } catch (Exception e) {
                        //if not initialized its not a problem
                    }
                }

            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt1.setText(new String("Backup as |SV at: " + file.toString()));

                //Write to file
                FileWriter fileWriter = null;
                try {

                    fileWriter = new FileWriter(file);

                    SMSHelpers smsHelpers = new SMSHelpers(MainActivity.this);
                    List<String> msgLst = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                        msgLst = smsHelpers.getAllSms(false);
                    } else {
                        msgLst = smsHelpers.getAllSmsAPIBefore19(false);
                    }

                    if (msgLst.size() <= 0) {
                        displayError("No SMS in Outbox");
                        return;
                    }

                    String delimiter = ";;;";
                    fileWriter.write("---"+delimiter+" Start of Messages "+delimiter+"---\n");
                    int column = 1;
                    for (String msg: msgLst) {
                        if (column > 3) {
                            column = 1;
                            fileWriter.append("\n");
                        }
                        fileWriter.append(msg + delimiter);
                        column += 1;
                    }
                    fileWriter.append("\n---"+delimiter+" End of messages "+delimiter+"---");

                } catch (IOException e) {
                    //Handle exception
                    displayError(e.getMessage().toString());
                } finally {
                    try {
                        fileWriter.close();
                    } catch (Exception e) {
                        //if not initialized its not a problem
                    }
                }

            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt1.setText(new String("Backup as ;;;SV at: " + file.toString()));

                //Write to file
                FileWriter fileWriter = null;
                try {

                    fileWriter = new FileWriter(file);

                    SMSHelpers smsHelpers = new SMSHelpers(MainActivity.this);
                    List<String> msgLst = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                        msgLst = smsHelpers.getAllSms(false);
                    } else {
                        msgLst = smsHelpers.getAllSmsAPIBefore19(false);
                    }

                    if (msgLst.size() <= 0) {
                        displayError("No SMS in Outbox");
                        return;
                    }

                    String delimiter = ";;;";
                    fileWriter.write("---"+delimiter+" Start of Messages "+delimiter+"---\n");
                    int column = 1;
                    for (String msg: msgLst) {
                        if (column > 3) {
                            column = 1;
                            fileWriter.append("\n");
                        }
                        fileWriter.append(msg + delimiter);
                        column += 1;
                    }
                    fileWriter.append("\n---"+delimiter+" End of messages "+delimiter+"---");

                } catch (IOException e) {
                    //Handle exception
                    displayError(e.getMessage().toString());
                } finally {
                    try {
                        fileWriter.close();
                    } catch (Exception e) {
                        //if not initialized its not a problem
                    }
                }

            }
        });


    }

    void displayError(String msg) {
        Toast.makeText(MainActivity.this, msg.substring(0, Toast.LENGTH_LONG), Toast.LENGTH_LONG).show();
        txt1.setText(msg);
    }

}
